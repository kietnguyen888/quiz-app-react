import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import Review from "../Review/Review";
import Timer from "../timer/Timer";
import questionStyle from "./question.module.css";
import Loading from "../../components/Loading";
export default function Question() {
  const [questions, setQuestions] = useState([]);
  const [showReview, setShowReview] = useState(false);
  const [loading, setLoading] = useState(true);
  const [answerChoose, setAnswerChoose] = useState("");
  const chosenList = useRef([]);
  const displayLoading = () => {
    setLoading(false);
  };
  useEffect(() => {
    axios({
      url: "https://quizapp-react-kiet.herokuapp.com/questionList",
      method: "GET",
    })
      .then((res) => {
        setQuestions(res.data);
        displayLoading();
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [showScore, setShowScore] = useState(false);
  const [score, setScore] = useState(0);

  const handleAnswerOptionClick = (answerOption) => {
    const chosenItem = chosenList.current.findIndex(
      (item) => item.id === questions[currentQuestion].id
    );

    if (chosenItem === -1) {
      chosenList.current.push({ ...questions[currentQuestion], answerOption });
    } else {
      chosenList.current[chosenItem] = {
        ...questions[currentQuestion],
        answerOption,
      };
    }
  };

  const displayAnswer = (answerChosen) => {
    const foughtAnswer = chosenList.current.find(
      (item) => item.answerOption.answer_content === answerChosen.answer_content
    );
    if (!foughtAnswer) {
      return "";
    } else {
      return "rgb(0, 183, 255)";
    }
  };

  const previousClick = () => {
    if (currentQuestion === 0) {
      return;
    }
    const previousQuestion = currentQuestion - 1;

    setCurrentQuestion(previousQuestion);
  };
  const nextClick = () => {
    const nextQuestion = currentQuestion + 1;
    if (nextQuestion < questions.length) {
      setCurrentQuestion(nextQuestion);
    }
  };
  const submitClick = () => {
    alert("Are you sure to submit");
    let tempScore = 0;
    chosenList.current.forEach((element) => {
      if (element.answerOption.correct === true) {
        tempScore++;
      }
    });
    setScore(tempScore);
    setShowScore(true);
  };
  const displayMain = () => {
    if (loading) {
      return (
        <Loading style={{ alignItems: "center", justifyContent: "center" }} />
      );
    } else if (showScore && !showReview) {
      return (
        <div className={questionStyle["question_container"]}>
          <div className={questionStyle["score_section"]}>
            <h2>
              You scored {score} out of {questions.length}
            </h2>
            <Link to="/">
              <button className={questionStyle["tryAgain_btn"]}>
                Try again
              </button>
            </Link>
            <button
              className={questionStyle["review_btn"]}
              style={{ display: showReview ? "none" : "" }}
              onClick={() => {
                setShowReview(true);
              }}
            >
              Review
            </button>
          </div>
        </div>
      );
    } else if (showReview) {
      return <Review questions={questions} chosenList={chosenList} />;
    } else if (!showScore) {
      return (
        <>
          <Timer hours="0" minutes="2" cb={setShowScore} />
          <div className={questionStyle["question_control"]}>
            <button
              className={questionStyle["previous_btn"]}
              disabled={currentQuestion === 0 ? true : false}
              onClick={previousClick}
            >
              Previous
            </button>

            {currentQuestion === questions.length - 1 ? (
              <button
                className={questionStyle["submit_btn"]}
                onClick={submitClick}
              >
                Submit
              </button>
            ) : (
              <button className={questionStyle["next_btn"]} onClick={nextClick}>
                Next
              </button>
            )}
          </div>
          <div className={questionStyle["question_container"]}>
            <div className={questionStyle["question_section"]}>
              <div className={questionStyle["question_count"]}>
                <span>Question {currentQuestion + 1}</span>/{questions.length}
              </div>
              <div className={questionStyle["question_text"]}>
                {questions[currentQuestion]?.question_content}
              </div>
            </div>
            <div className={questionStyle["answer_section"]}>
              {questions[currentQuestion]?.answers.map(
                (answerOption, indexAnswer) => (
                  <button
                    style={{ backgroundColor: displayAnswer(answerOption) }}
                    key={answerOption.answer_content}
                    onClick={() => {
                      setAnswerChoose(answerOption);
                      handleAnswerOptionClick(answerOption);
                    }}
                  >
                    {answerOption.answer_content}
                  </button>
                )
              )}
            </div>
          </div>
        </>
      );
    }
  };
  return <>{displayMain()}</>;
}
