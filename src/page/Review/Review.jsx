import React, { useState } from "react";
import { Link } from "react-router-dom";
import reviewStyle from "./reviewStyle.module.css";
export default function Review(props) {
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const previousClick = () => {
    if (currentQuestion === 0) {
      return;
    }
    const previousQuestion = currentQuestion - 1;

    setCurrentQuestion(previousQuestion);
  };
  const nextClick = () => {
    const nextQuestion = currentQuestion + 1;
    if (nextQuestion < props.questions.length) {
      setCurrentQuestion(nextQuestion);
    }
  };

  const displayAnswer = (answerChosen) => {
    const foughtAnswer = props.chosenList.current.find(
      (item) => item.answerOption.answer_content === answerChosen.answer_content
    );
    if (!foughtAnswer && answerChosen?.correct) {
      return "#10B981";
    } else if (foughtAnswer?.answerOption?.correct === true) {
      return "#10B981";
    } else if (foughtAnswer?.answerOption?.correct === false) {
      return "red";
    }
  };

  return (
    <div className={reviewStyle['review_container']}>
      <div className={reviewStyle['question_control']}>
        <button
        className={reviewStyle['previous_btn']}
          disabled={currentQuestion === 0 ? true : false}
          onClick={previousClick}
        >
          Previous
        </button>
        <button className={reviewStyle['next_btn']} onClick={nextClick}>Next</button>

        <Link to="/">
          <button className={reviewStyle['tryAgain_btn']}>Try again</button>
        </Link>
      </div>
      <div className={reviewStyle['question_section']}>
        <div className={reviewStyle['question_count']}>
          <span>Question {currentQuestion + 1}</span>/{props.questions.length}
        </div>
        <div className={reviewStyle['question_text']}>
          {props.questions[currentQuestion]?.question_content}
        </div>
      </div>
      <div className={reviewStyle['answer_section']}>
        {props.questions[currentQuestion]?.answers.map(
          (answerOption, indexAnswer) => (
            <button
              disabled="disabled"
              style={{ backgroundColor: displayAnswer(answerOption) }}
              key={answerOption.answer_content}
            >
              {answerOption.answer_content}
            </button>
          )
        )}
      </div>
    </div>
  );
}
