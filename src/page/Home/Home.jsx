import React from 'react'
import { Link } from 'react-router-dom'
import homeStyle from "./home.module.css"
export default function Home() {
  return (
    <div>
        <h1 className={homeStyle.heading}>Welcome To Quiz App!</h1>
        <Link to="/question"><button className={homeStyle.start_btn}>Start!</button></Link>
    </div>
  )
}
