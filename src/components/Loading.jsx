import React from 'react'
import loading from "../assets/loading.gif"
export default function Loading() {
  return (
    <img style={{width:200}} src={loading} alt="loading-img" />
  )
}
