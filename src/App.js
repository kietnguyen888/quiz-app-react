import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./page/Home/Home";
import Question from "./page/Question/Question"
export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" exact element={<Home/>} />
        <Route path="/question" element={<Question/>}/>
      </Routes>
    </BrowserRouter>
  );
}
